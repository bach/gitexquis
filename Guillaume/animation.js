  (function(){
    window.onload = function(){
      var w_screen = document.body.clientWidth, h_screen = document.body.clientHeight;

      var img = document.getElementById('animation');
      var w_img=document.images["image"].width, h_img=document.images["image"].height;
      img.style.position='absolute';
      img.style.left=(w_screen-w_img)/12+'px';
      var y=-h_img;
      setInterval(function(){
        img.style.top=(y+=5)+'px';
        if(parseInt(img.style.top)>h_screen) y=-h_img;
      }, 17);

      var texte = document.getElementById('anime_text');
      texte.style.position='absolute';
      var w_div = texte.offsetWidth;
      var x = -w_div;
      setInterval(function(){
        texte.style.left=(x+=4)+'px';
        if(parseInt(texte.style.left)>w_screen) x=-w_div;
      }, 30);
    };
  })();
