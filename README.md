# GitExquis

1. ACTUALISER (vers notre ordinateur): ```git pull origin master```
2. VOIR LES MODIFICATIONS: ```git status```
3. AJOUTER CES MODIFICATIONS: ```git add --all```
4. VALIDER LES MODIFICATIONS: ```git commit -m "MESSAGE"```
5. ACTUALISER (vers le GIT): ```git push origin master```


# Rotations CadavreExquis

https://esadhar.net/pad/mypads/?/mypads/group/2019-2020-ao2sw75/pad/view/gitexquis-rotation-wkxbwb7
